/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.shapeproject;

/**
 *
 * @author f
 */
public class TestSquare {
    public static void main(String[] args) {
        Square square1 = new Square(4);
        System.out.println("Area of Square(s = "+square1.getS()+") is "+square1.calArea());
        square1.setS(2);
        System.out.println("Area of Square(s = "+square1.getS()+") is "+square1.calArea());
        square1.setS(0);
        System.out.println("Area of Square(s = "+square1.getS()+") is "+square1.calArea());
    }
}
